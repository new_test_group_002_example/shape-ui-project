/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kongphop.shapeui.project;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Admin
 */
public class RectangleFrame extends JFrame {

    JLabel lblWidth;
    JLabel lblLength;
    JTextField txtWidth;
    JTextField txtLength;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(500, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);
        
        lblLength = new JLabel("length:", JLabel.TRAILING);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5, 30);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        this.add(lblLength);
        
        txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 30);
        this.add(txtLength);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(120, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width= ??? Rectangle length= ??? "
                + "area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 60);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strLength = txtLength.getText();
                    double width = Double.parseDouble(strWidth);
                    double length = Double.parseDouble(strLength);
                    Rectangle rectangle = new Rectangle(width, length);
                    lblResult.setText("Rectangle w = " + String.format("%.2f", rectangle.getWidth())
                            + " Rectangle l = " + String.format("%.2f", rectangle.getLength())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtLength.setText("");
                    txtLength.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
